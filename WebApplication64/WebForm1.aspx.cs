﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Configuration;
using System.Text;
using System.Net;
using System.Net.Mail;
using System.Data;
using System.Data.OleDb;
using System.Threading.Tasks;

namespace WebApplication64
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void king_Click(object sender, EventArgs e)
        {
            OleDbConnection con = new OleDbConnection("provider=microsoft.ace.oledb.12.0;data source=E:\\Email.xls;extended properties =excel 12.0");
            con.Open();
            OleDbCommand cmd = new OleDbCommand("Select Email,Name from [sheet1$]", con);
            OleDbDataAdapter adp = new OleDbDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            GridView1.DataSource = ds;
            GridView1.DataBind();
            Label2.Text = GridView1.Rows.Count.ToString();
            con.Close();
        }

        protected void Sendbtn_Click(object sender, EventArgs e)
        {
            List<GridViewRow> list = new List<GridViewRow>();
            foreach (GridViewRow row in GridView1.Rows)
            {
                list.Add(row);
            }
            System.Diagnostics.Stopwatch sWatch = new System.Diagnostics.Stopwatch();

            sWatch.Start();
            Parallel.ForEach(list, row =>
            {

                string Emails = row.Cells[0].Text.Trim();

                string file = Server.MapPath("~/Mail.html");
                string mailbody = System.IO.File.ReadAllText(file);
                string to = Emails;
                string from = "abc@gmail.com";
                MailMessage msg = new MailMessage(from, to);
                msg.Subject = "Auto Response Email";
                msg.Body = mailbody;
                msg.BodyEncoding = Encoding.UTF8;
                msg.IsBodyHtml = true;
                SmtpClient client = new SmtpClient("smtp.gmail.com", 25);
                client.UseDefaultCredentials = false;
                System.Net.NetworkCredential basicCredential = new System.Net.NetworkCredential("abc@gmail.com", "*****");
                client.EnableSsl = true;
                client.UseDefaultCredentials = true;
                client.Credentials = basicCredential;
                try
                {
                    client.Send(msg);
                    cnfrm.Text = "Email Sended Successfully";
                }
                catch (Exception ex)
                {
                    Response.Write(ex.Message);
                }
            });
            sWatch.Stop();
            lblShowTime.Text = sWatch.ElapsedMilliseconds.ToString();
        }
    }
}